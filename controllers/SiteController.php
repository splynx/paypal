<?php

namespace app\controllers;

use app\components\ValidateConfig;
use app\models\PayInvoiceForm;
use app\models\PaymentForm;
use app\models\PayRequestForm;
use splynx\models\Customer;
use splynx\models\finance\Invoices;
use splynx\models\finance\Requests;
use yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        ValidateConfig::checkAll();

        $model = new PaymentForm();
        $customer = Yii::$app->getSession()->get('splynx_customer_id', null);

        if ($customer == null) {
            return $this->redirect('/portal/');
        }
        $model->customer = $customer;

        if (Yii::$app->getRequest()->getIsPost()) {
            $post = Yii::$app->getRequest()->post();

            if ($model->load($post) === false and isset($post['amount'])) {
                $model->amount = $post['amount'];
            }

            if ($model->validate() and ($url = $model->create()) !== false) {
                return $this->redirect($url);
            } else {
                return $this->render('index', [
                    'model' => $model
                ]);
            }
        } else {
            return $this->render('index', [
                'model' => $model
            ]);
        }
    }

    public function actionResult($status, $token, $paymentId = null, $PayerID = null)
    {
        ValidateConfig::checkAll();

        $model = new PaymentForm();
        $customer = Yii::$app->getSession()->get('splynx_customer_id', null);
        if ($customer == null) {
            return $this->redirect('/portal/');
        }
        $model->customer = $customer;

        if ($status == 'cancel') {
            $model->cancel();
            Yii::$app->getSession()->setFlash('warning', 'Payment was cancelled!');
        } elseif ($status == 'success') {
            if ($model->process($paymentId, $PayerID) === false) {
                Yii::$app->getSession()->setFlash('danger', $model->error);
            } else {
                Yii::$app->getSession()->setFlash('success', 'Payment successful!');
            }
        } elseif ($status == 'error') {
            $model->error();
            Yii::$app->getSession()->setFlash('danger', 'Payment error!');
        } else {
            Yii::$app->getSession()->setFlash('danger', 'Unknown error!');
        }

        return $this->render('result', [
            'url' => '/portal/finance/payments/list'
        ]);
    }

    public function actionPayInvoice($module = null, $model = null, $item_id = null, $status = null, $token = null, $paymentId = null, $PayerID = null)
    {
        ValidateConfig::checkAll();

        $model = new PayInvoiceForm();

        $customer = Yii::$app->getSession()->get('splynx_customer_id', null);
        if ($customer == null) {
            return $this->redirect('/portal/');
        }

        $customerPartner = Customer::findById($customer)->partner_id;

        // Find invoice
        $invoice = Invoices::findByNumber($item_id);
        if ($invoice == null or $invoice->customer_id != $customer) {
            throw new yii\base\InvalidParamException('Invalid invoice!');
        }
        $invoice = Invoices::findById($invoice->id);
        $model->invoice = $invoice;

        // If `status` is set - process payment
        if ($status !== null) {
            if ($status == 'cancel') {
                $model->cancel();
                Yii::$app->getSession()->setFlash('warning', 'Payment was cancelled!');
            } elseif ($status == 'success') {
                if ($model->process($paymentId, $PayerID) === false) {
                    Yii::$app->getSession()->setFlash('danger', $model->error);
                } else {
                    Yii::$app->getSession()->setFlash('success', 'Payment successful!');
                }
            } elseif ($status == 'error') {
                $model->error();
                Yii::$app->getSession()->setFlash('danger', 'Payment error!');
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Unknown error!');
            }

            return $this->render('result', [
                'url' => '/portal/finance/invoices'
            ]);
        }

        $processToPayment = true;

        // Check invoice status
        if ($invoice->status !== Invoices::STATUS_NOT_PAID) {
            Yii::$app->getSession()->setFlash('success', 'Invoice already paid!');
            return $this->render('result', []);
        }

        if ($processToPayment) {
            $url = $model->create();
        } else {
            $url = '';
        }

        if (isset(Yii::$app->params['partnerCurrency'][$customerPartner])) {
            $currency = Yii::$app->params['partnerCurrency'][$customerPartner];
        } else {
            $currency = Yii::$app->params['currency'];
        }

        return $this->render('pay-invoice', [
            'process_to_payment' => $processToPayment,
            'model' => $model,
            'invoice' => $invoice,
            'currency' => $currency,
            'payment_url' => $url
        ]);
    }

    public function actionPayRequest($module = null, $model = null, $item_id = null, $status = null, $token = null, $paymentId = null, $PayerID = null)
    {
        ValidateConfig::checkAll();

        $model = new PayRequestForm();
        $customer = Yii::$app->getSession()->get('splynx_customer_id', null);
        if ($customer == null) {
            return $this->redirect('/portal/');
        }

        $customerPartner = Customer::findById($customer)->partner_id;

        // Find request
        $request = Requests::findByNumber($item_id);
        if ($request == null or $request->customer_id != $customer) {
            throw new yii\base\InvalidParamException('Invalid request!');
        }
        $request = Requests::findById($request->id);
        $model->request = $request;

        // If `status` is set - process payment
        if ($status !== null) {
            if ($status == 'cancel') {
                $model->cancel();
                Yii::$app->getSession()->setFlash('warning', 'Payment was cancelled!');
            } elseif ($status == 'success') {
                if ($model->process($paymentId, $PayerID) === false) {
                    Yii::$app->getSession()->setFlash('danger', $model->error);
                } else {
                    Yii::$app->getSession()->setFlash('success', 'Payment successful!');
                }
            } elseif ($status == 'error') {
                $model->error();
                Yii::$app->getSession()->setFlash('danger', 'Payment error!');
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Unknown error!');
            }

            return $this->render('result', [
                'url' => '/portal/finance/requests'
            ]);
        }

        $processToPayment = true;

        // Check request status
        if ($request->status !== Requests::STATUS_NOT_PAID) {
            Yii::$app->getSession()->setFlash('success', 'Request already paid!');
            return $this->render('result', []);
        }

        if ($processToPayment) {
            $url = $model->create();
        } else {
            $url = '';
        }

        if (isset(Yii::$app->params['partnerCurrency'][$customerPartner])) {
            $currency = Yii::$app->params['partnerCurrency'][$customerPartner];
        } else {
            $currency = Yii::$app->params['currency'];
        }

        return $this->render('pay-request', [
            'process_to_payment' => $processToPayment,
            'model' => $model,
            'request' => $request,
            'currency' => $currency,
            'payment_url' => $url
        ]);
    }
}
