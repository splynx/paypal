<?php

namespace app\models;

use app\components\PayPal;
use splynx\models\Customer;
use splynx\models\finance\Payment;
use splynx\models\finance\Transactions;
use Yii;
use yii\base\Model;

/**
 * Class PaymentForm
 * @package app\models
 *
 * @property float $amount
 */
class PaymentForm extends Model
{

    public $customer;
    public $amount;

    public $result;
    // PayPal execute error
    public $error;

    const SESSION_PAYMENT_ID = 'paypal_payment_id';
    const BANK_STATEMENT_ID = 'bank_statement_id';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer', 'amount'], 'required'],
            [['customer'], 'integer'],
            [['amount'], 'number', 'min' => 0.01]
        ];
    }

    public function getFee()
    {
        return Yii::$app->params['serviceFee'];
    }

    public function isFee()
    {
        return $this->getFee() !== 0;
    }

    public function getTotalAmount()
    {
        return $this->amount + $this->getFeeAmount();
    }

    public function getFeeAmount()
    {
        if ($this->isFee()) {
            return $this->amount / 100 * $this->getFee();
        } else {
            return 0;
        }
    }

    public function create($isDirect = false)
    {
        $payPal = new PayPal(['customer_id' => $this->customer]);

        // Create bank statement
        $bankStatement = new BankStatement();
        $bankStatement->customer_id = $this->customer;
        $bankStatement->amount = $this->getTotalAmount();
        $bankStatement->payment_date = date('Y-m-d');

        if ($bankStatement->create() === false) {
            $this->addError('result', 'Error adding bank statement!');
            return false;
        }

        $title = 'Payment #' . $bankStatement->id;

        $customer = Yii::$app->getSession()->get('splynx_customer_id', null);
        $partner = Customer::findById($customer)->partner_id;

        if ($payPal->create($this->getTotalAmount(), $title, 'payment', $partner, $isDirect)) {
            // Save payment id to session
            Yii::$app->session->set(self::SESSION_PAYMENT_ID, $payPal->paymentId);
            Yii::$app->session->set(self::BANK_STATEMENT_ID, $bankStatement->id);

            // Return PayPal payment url
            return $payPal->redirectUrl;
        } else {
            return false;
        }
    }

    public function process($paymentId, $PayerID)
    {
        $result = true;
        $savedPaymentId = Yii::$app->session->get(self::SESSION_PAYMENT_ID);

        if ($paymentId == $savedPaymentId) {
            $bankStatementId = Yii::$app->session->get(self::BANK_STATEMENT_ID);

            $bankStatement = BankStatement::get($bankStatementId);

            if ($bankStatement !== null and $bankStatement->status == BankStatement::STATUS_NEW) {
                $payPal = new PayPal(['customer_id' => $this->customer]);
                if ($payPal->execute($paymentId, $PayerID)) {
                    if ($this->isFee()) {
                        // Set amounts
                        $this->amount = $payPal->getAmount() - ($payPal->getAmount() / (100 / $this->getFee()));
                    } else {
                        $this->amount = $payPal->getAmount();
                    }

                    // Create Splynx payment
                    $payment = new Payment();
                    $payment->customer_id = $this->customer;
                    $payment->payment_type = Yii::$app->params['payment_method_id'];
                    $payment->amount = $payPal->getAmount();
                    $payment->comment = 'Pay by PayPal';
                    $payment->field_4 = 'Payment: ' . $paymentId;
                    $payment->field_5 = 'Bank Statement: ' . $bankStatementId;

                    $createdPaymentId = $payment->create();

                    // Set transaction for fee
                    if ($this->isFee()) {
                        $transaction = new Transactions();
                        $transaction->customer_id = $this->customer;
                        $transaction->type = Transactions::TYPE_DEBIT;
                        $transaction->price = $this->getFeeAmount();
                        $transaction->category = Yii::$app->params['transaction_fee_category'];
                        $transaction->description = 'PayPal fee (for payment #' . $createdPaymentId . ')';
                        $transaction->source = Transactions::SOURCE_MANUAL;

                        $transaction->create();
                    }

                    $bankStatement->status = BankStatement::STATUS_PROCESSED;

                } else {
                    $bankStatement->status = BankStatement::STATUS_ERROR;
                    $this->error = $this->getError($payPal->error);
                    $result = false;
                }

                // Update bank statement
                $bankStatement->update();
            }
        }

        return $result;
    }

    public function cancel()
    {
        $bankStatement = BankStatement::get(Yii::$app->session->get(self::BANK_STATEMENT_ID));
        if ($bankStatement !== null AND $bankStatement->status == BankStatement::STATUS_NEW) {
            $bankStatement->status = BankStatement::STATUS_CANCELED;
            $bankStatement->update();
        }
    }

    public function error()
    {
        $bankStatement = BankStatement::get(Yii::$app->session->get(self::BANK_STATEMENT_ID));
        if ($bankStatement !== null AND $bankStatement->status == BankStatement::STATUS_NEW) {
            $bankStatement->status = BankStatement::STATUS_ERROR;
            $bankStatement->update();
        }
    }

    private function getError($error)
    {
        return $error . ' Please, contact support!';
    }
}
