<?php

namespace app\models;

use app\components\PayPal;
use splynx\models\Customer;
use splynx\models\finance\Invoices;
use splynx\models\finance\Payment;
use Yii;
use yii\base\Model;

class PayInvoiceForm extends Model
{
    /** @var Invoices */
    public $invoice;
    // PayPal execute error
    public $error;

    const SESSION_PAYMENT_ID = 'invoice_paypal_payment_id';
    const BANK_STATEMENT_ID = 'invoice_bank_statement_id';

    /**
     * @return Invoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param Invoices $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    public function getFee()
    {
        return Yii::$app->params['serviceFee'];
    }

    public function isFee()
    {
        return $this->getFee() !== 0;
    }

    public function getTotalAmount()
    {
        return round($this->getInvoice()->total + $this->getFeeAmount(), 2);
    }

    public function getAmount()
    {
        return round($this->getInvoice()->total, 2);
    }

    public function getFeeAmount()
    {
        if ($this->isFee()) {
            return round($this->getInvoice()->total / 100 * $this->getFee(), 2);
        } else {
            return 0;
        }
    }

    /**
     * @param bool $isDirect
     * @return bool
     */
    public function create($isDirect = false)
    {
        $customer_id = $this->getInvoice()->customer_id;
        $payPal = new PayPal(['customer_id' => $customer_id]);

        // Create bank statement
        $bankStatement = new BankStatement();
        $bankStatement->customer_id = $customer_id;
        $bankStatement->amount = $this->getTotalAmount();
        $bankStatement->payment_date = date('Y-m-d');

        if ($bankStatement->create() === false) {
            $this->addError('result', 'Error adding bank statement!');
            return false;
        }

        $title = 'Invoice #' . $this->getInvoice()->number;

        $customer = Yii::$app->getSession()->get('splynx_customer_id', null);
        $partner = Customer::findById($customer)->partner_id;

        if ($payPal->create($this->getTotalAmount(), $title, 'invoice', $partner, $isDirect, $this->getInvoice()->number)) {
            // Save payment id to session
            Yii::$app->session->set(self::SESSION_PAYMENT_ID, $payPal->paymentId);
            Yii::$app->session->set(self::BANK_STATEMENT_ID, $bankStatement->id);

            // Return PayPal payment url
            return $payPal->redirectUrl;
        } else {
            return false;
        }
    }

    public function process($paymentId, $PayerID)
    {
        $result = true;
        $savedPaymentId = Yii::$app->session->get(self::SESSION_PAYMENT_ID);
        $invoice = $this->getInvoice();

        if ($paymentId == $savedPaymentId) {
            $bankStatementId = Yii::$app->session->get(self::BANK_STATEMENT_ID);

            $bankStatement = BankStatement::get($bankStatementId);

            if ($invoice !== null and $invoice->status == Invoices::STATUS_NOT_PAID) {
                $payPal = new PayPal(['customer_id' => $invoice->customer_id]);
                if ($payPal->execute($paymentId, $PayerID)) {
                    if ($this->isFee()) {
                        //Add item with fee to invoice
                        $fee_item = [
                            [
                                'price' => ($this->isFeeVAT()) ? $this->getFeeWithoutVAT() : $this->getFeeAmount(),
                                'description' => isset(Yii::$app->params['fee_message']) ? Yii::$app->params['fee_message'] : 'Commission',
                                'quantity' => 1,
                                'tax' => ($this->isFeeVAT()) ? Yii::$app->params['fee_VAT'] : 0,
                            ],
                        ];

                        $params['items'] = array_merge($invoice->items, $fee_item);
                        $invoice->update($params);
                    }
                    // Create Splynx payment
                    $payment = new Payment();
                    $payment->customer_id = $invoice->customer_id;
                    $payment->invoice_id = $invoice->id;
                    $payment->payment_type = Yii::$app->params['payment_method_id'];
                    $payment->amount = $payPal->getAmount();
                    $payment->comment = 'Pay by PayPal';
                    $payment->field_4 = 'Payment: ' . $paymentId;
                    $payment->field_5 = 'Invoice: ' . $invoice->number;

                    $payment->create();

                    $bankStatement->status = BankStatement::STATUS_PROCESSED;
                } else {
                    $bankStatement->status = BankStatement::STATUS_ERROR;
                    $this->error = $this->getError($payPal->error);
                    $result = false;
                }
                // Update bank statement
                $bankStatement->update();
            }
        }

        return $result;
    }

    public function cancel()
    {
        $bankStatement = BankStatement::get(Yii::$app->session->get(self::BANK_STATEMENT_ID));
        if ($bankStatement !== null and $bankStatement->status == BankStatement::STATUS_NEW) {
            $bankStatement->status = BankStatement::STATUS_CANCELED;
            $bankStatement->update();
        }
    }

    public function error()
    {
        $bankStatement = BankStatement::get(Yii::$app->session->get(self::BANK_STATEMENT_ID));
        if ($bankStatement !== null and $bankStatement->status == BankStatement::STATUS_NEW) {
            $bankStatement->status = BankStatement::STATUS_ERROR;
            $bankStatement->update();
        }
    }

    public function isFeeVAT()
    {
        if (isset(Yii::$app->params['fee_VAT'])) {
            return Yii::$app->params['fee_VAT'] !== 0 and is_numeric(Yii::$app->params['fee_VAT']);
        } else {
            return false;
        }
    }

    public function getFeeWithoutVAT()
    {
        return $this->getFeeAmount() / (100 + Yii::$app->params['fee_VAT']) * 100;
    }

    private function getError($error)
    {
        return $error . ' Please, contact support!';
    }
}
