<?php

namespace app\models;

use app\components\PayPal;
use splynx\models\Customer;
use splynx\models\finance\Payment;
use splynx\models\finance\Requests;
use splynx\models\finance\Transactions;
use Yii;
use yii\base\Model;

class PayRequestForm extends Model
{
    /** @var Requests */
    public $request;
    // PayPal execute error
    public $error;

    const SESSION_PAYMENT_ID = 'request_paypal_payment_id';
    const BANK_STATEMENT_ID = 'request_bank_statement_id';

    /**
     * @return Requests
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Requests $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getFee()
    {
        return Yii::$app->params['serviceFee'];
    }

    public function isFee()
    {
        return $this->getFee() !== 0;
    }

    public function getTotalAmount()
    {
        return round($this->getRequest()->total + $this->getFeeAmount(), 2);
    }

    public function getAmount()
    {
        return round($this->getRequest()->total, 2);
    }

    public function getFeeAmount()
    {
        if ($this->isFee()) {
            return round($this->getRequest()->total / 100 * $this->getFee(), 2);
        } else {
            return 0;
        }
    }

    /**
     * @param bool $isDirect
     * @return bool
     */
    public function create($isDirect = false)
    {
        $customer_id = $this->getRequest()->customer_id;
        $payPal = new PayPal(['customer_id' => $customer_id]);

        // Create bank statement
        $bankStatement = new BankStatement();
        $bankStatement->customer_id = $customer_id;
        $bankStatement->amount = $this->getTotalAmount();
        $bankStatement->payment_date = date('Y-m-d');

        if ($bankStatement->create() === false) {
            $this->addError('result', 'Error adding bank statement!');
            return false;
        }

        $title = 'Request #' . $this->getRequest()->number;

        $customer = Yii::$app->getSession()->get('splynx_customer_id', null);
        $partner = Customer::findById($customer)->partner_id;

        if ($payPal->create($this->getTotalAmount(), $title, 'request', $partner, $isDirect, $this->getRequest()->number)) {
            // Save payment id to session
            Yii::$app->session->set(self::SESSION_PAYMENT_ID, $payPal->paymentId);
            Yii::$app->session->set(self::BANK_STATEMENT_ID, $bankStatement->id);

            // Return PayPal payment url
            return $payPal->redirectUrl;
        } else {
            return false;
        }
    }

    public function process($paymentId, $PayerID)
    {
        $result = true;
        $savedPaymentId = Yii::$app->session->get(self::SESSION_PAYMENT_ID);
        $request = $this->getRequest();

        if ($paymentId == $savedPaymentId) {
            $bankStatementId = Yii::$app->session->get(self::BANK_STATEMENT_ID);

            $bankStatement = BankStatement::get($bankStatementId);

            if ($request !== null and $request->status == Requests::STATUS_NOT_PAID) {
                $payPal = new PayPal(['customer_id' => $request->customer_id]);
                if ($payPal->execute($paymentId, $PayerID)) {
                    if ($this->isFee() and isset(Yii::$app->params['add_fee_request']) and Yii::$app->params['add_fee_request'] === true) {
                        //Add item with fee to request
                        $fee_item = [
                            [
                                'price' => ($this->isFeeVAT()) ? $this->getFeeWithoutVAT() : $this->getFeeAmount(),
                                'description' => Yii::$app->params['fee_message'],
                                'quantity' => 1,
                                'tax' => ($this->isFeeVAT()) ? Yii::$app->params['fee_VAT'] : 0,
                            ],
                        ];

                        $params['items'] = array_merge($request->items, $fee_item);
                        $request->update($params);
                    }
                    // Create Splynx payment
                    $payment = new Payment();
                    $payment->customer_id = $request->customer_id;
                    $payment->request_id = $request->id;
                    $payment->payment_type = Yii::$app->params['payment_method_id'];
                    $payment->amount = $payPal->getAmount();
                    $payment->comment = 'Pay by PayPal';
                    $payment->field_4 = 'Payment: ' . $paymentId;
                    $payment->field_5 = 'Request: ' . $request->number;

                    $createdPaymentId = $payment->create();

                    // Set transaction for fee
                    if ($this->isFee()) {
                        $transaction = new Transactions();
                        $transaction->customer_id = $request->customer_id;
                        $transaction->type = Transactions::TYPE_DEBIT;
                        $transaction->price = $this->getFeeAmount();
                        $transaction->category = Yii::$app->params['transaction_fee_category'];
                        $transaction->description = 'PayPal fee (for payment #' . $createdPaymentId . ')';
                        $transaction->source = Transactions::SOURCE_MANUAL;

                        $transaction->create();
                    }

                    $bankStatement->status = BankStatement::STATUS_PROCESSED;
                } else {
                    $bankStatement->status = BankStatement::STATUS_ERROR;
                    $this->error = $this->getError($payPal->error);
                    $result = false;
                }

                // Update bank statement
                $bankStatement->update();
            }
        }

        return $result;
    }

    public function cancel()
    {
        $bankStatement = BankStatement::get(Yii::$app->session->get(self::BANK_STATEMENT_ID));
        if ($bankStatement !== null and $bankStatement->status == BankStatement::STATUS_NEW) {
            $bankStatement->status = BankStatement::STATUS_CANCELED;
            $bankStatement->update();
        }
    }

    public function error()
    {
        $bankStatement = BankStatement::get(Yii::$app->session->get(self::BANK_STATEMENT_ID));
        if ($bankStatement !== null and $bankStatement->status == BankStatement::STATUS_NEW) {
            $bankStatement->status = BankStatement::STATUS_ERROR;
            $bankStatement->update();
        }
    }

    public function getFeeWithoutVAT()
    {
        return $this->getFeeAmount() / (100 + Yii::$app->params['fee_VAT']) * 100;
    }

    public function isFeeVAT()
    {
        return Yii::$app->params['fee_VAT'] !== 0 and is_numeric(Yii::$app->params['fee_VAT']);
    }

    private function getError($error)
    {
        return $error . ' Please, contact support!';
    }
}
