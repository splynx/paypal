<?php

namespace app\commands;

use splynx\helpers\IPHelper;
use yii\console\Controller;

class InstallController extends Controller
{
    public function actionIndex()
    {
        echo 'Start install..' . "\n";

        $baseDir = \Yii::$app->getBasePath();

        // Chmod
        $paths = [
            $baseDir . DIRECTORY_SEPARATOR . 'runtime' => '0777',
            $baseDir . DIRECTORY_SEPARATOR . 'web/assets' => '0777',
            $baseDir . DIRECTORY_SEPARATOR . 'yii' => '0755',
            $baseDir . DIRECTORY_SEPARATOR . 'runtime/logs' => '0777'
        ];
        self::chmodFiles($paths);

        // Set Splynx dir
        $splynxDir = '/var/www/splynx/';

        // Create API key
        $apiKeyId = (int)exec($splynxDir . 'system/script/addon add-or-get-api-key --title="Splynx PayPal add-on"');
        if (!$apiKeyId) {
            exit("Error: Create API key failed!\n");
        }

        // Set permissions
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\BankStatements" --action="add" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\BankStatements" --action="index" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\BankStatements" --action="view" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\BankStatementsRecords" --action="add" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\BankStatementsRecords" --action="index" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\BankStatementsRecords" --action="update" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\BankStatementsRecords" --action="view" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Payments" --action="add" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Payments" --action="index" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Invoices" --action="index" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Invoices" --action="view" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Invoices" --action="update" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Transactions" --action="index" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Transactions" --action="add" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Requests" --action="index" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Requests" --action="view" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\finance\Requests" --action="update" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\customers\Customer" --action="index" --rule="allow"');
        exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\customers\Customer" --action="view" --rule="allow"');

        // Get Api Key key and secret
        $result = exec($splynxDir . 'system/script/addon get-api-key-and-secret --id=' . $apiKeyId);
        if (!$result) {
            exit("Error: Get API key anf secret failed!\n");
        }
        list($apiKeyKey, $apiKeySecret) = explode(',', $result);

        // Add IPs to white list
        $ips = array_keys(IPHelper::getIpsArray());
        $baseIP = reset($ips);
        exec($splynxDir . 'system/script/addon api-key-white-list --id=' . $apiKeyId . ' --list="' . implode(',', $ips) . '"');

        //
        // Add module
        //
        $moduleName = 'splynx_paypal_addon';
        $result = exec($splynxDir . 'system/script/addon add-or-get-module --module="' . $moduleName . '" --title="Splynx PayPal add-on"');

        if (trim($result) != $moduleName) {
            exit('Error adding Splynx PayPal add-on');
        }

        // Add module entry points
        $points = [
            [
                'name' => 'paypal_pay_invoice_point',
                'place' => 'portal',
                'model' => 'Invoices',
                'url' => '/paypal/pay-invoice'
            ],
            [
                'name' => 'paypal_pay_request_point',
                'place' => 'portal',
                'model' => 'Requests',
                'url' => '/paypal/pay-request'
            ]
        ];

        foreach ($points as $point) {
            $result = exec($splynxDir . 'system/script/addon add-or-get-entry-point \
            --type="action_link" \
            --module="' . $moduleName . '" \
            --place="' . $point['place'] . '" \
            --model="' . $point['model'] . '" \
            --title="Pay by PayPal" \
            --icon="fa-paypal" \
            --url="' . urlencode($point['url']) . '" \
            --name="' . $point['name'] . '"');

            if (!$result) {
                exit('Error: Add entry point failed!' . "\n");
            }
        }

        // Check if already installed
        $paramsFilePath = $baseDir . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.php';
        if (file_exists($paramsFilePath)) {
            exit('Params file already exists! Terminate!' . "\n");
        }

        // Generate config
        $baseParamsFile = \Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.example.php';
        $params = file_get_contents($baseParamsFile);

        // Set Api host to config
        $params = preg_replace('/(("|\')api_domain("|\')\s*=>\s*)(""|\'\')/', "\\1'http://$baseIP/'", $params);

        // Set Api key to config
        $params = preg_replace('/(("|\')api_key("|\')\s*=>\s*)(""|\'\')/', "\\1'$apiKeyKey'", $params);

        // Set Api secret to config
        $params = preg_replace('/(("|\')api_secret("|\')\s*=>\s*)(""|\'\')/', "\\1'$apiKeySecret'", $params);

        // Cookie
        $cookieValidationKey = \Yii::$app->getSecurity()->generateRandomString();
        $params = preg_replace('/(("|\')cookieValidationKey("|\')\s*=>\s*)(""|\'\')/', "\\1'$cookieValidationKey'", $params);

        // Save config
        file_put_contents($paramsFilePath, $params);

        // Reload nginx
        if (file_exists('/etc/init.d/nginx')) {
            exec('/etc/init.d/nginx restart  > /dev/null 2>&1 3>/dev/null');
        }

        // Reload apache
        if (file_exists('/etc/init.d/apache2')) {
            exec('/etc/init.d/apache2 restart  > /dev/null 2>&1 3>/dev/null');
        }

        echo 'Installed!' . "\n";
    }

    protected static function chmodFiles($paths)
    {
        foreach ($paths as $path => $permission) {
            echo "chmod('$path', $permission)...";
            if (is_dir($path) || is_file($path)) {
                try {
                    if (chmod($path, octdec($permission))) {
                        echo "done.\n";
                    };
                } catch (\Exception $e) {
                    echo $e->getMessage() . "\n";
                }
            } else {
                echo "file not found.\n";
            }
        }
    }
}
