<?php

return [
    // API
    'api_domain' => '',
    'api_key' => '',
    'api_secret' => '',

    // Splynx url (without last slash)
    'splynx_url' => '',

    // PayPal credentials
    // You can get it by creating REST API app on page "My Apps & Credentials" in PayPal developer
    // account (https://developer.paypal.com/developer/applications/)
    'clientId' => '',
    'clientSecret' => '',
    'isProduction' => true,
    'currency' => 'USD',


    // You can set clientId and clientSecret for separate partner.
    // If partner not set in this array - default PayPal credentials will be used.
    // Structure: array key - is partner_id, value of array is clientId and clientSecret
    /*
    'partners_account' => [
        1 => [
            'clientId' => '',
            'clientSecret' => ''
        ]
    ],
    */

    // You can declare partnerCurrency for set currency to each of partners
    // For example, where 0,1,2,n partner id
    /*
    'partnerCurrency' => [
        1 => 'USD',
        2 => 'EUR',
        3 => 'UAH'
    ],
    */

    // Get payment method "PayPal" at `Config / Finance / Payment methods` and enter it's id here
    // Or simply use existing method id
    'payment_method_id' => 4,

    // Service fee (%)
    'serviceFee' => 0,

    // Add fee (position) to request
    'add_fee_request' => false,

    // If you set service add fee to invoice - set description for Invoice item
    'fee_message' => 'PayPal commission',

    // Fee VAT (%)
    'fee_VAT' => 0,

    // If you set service fee - set id for category of fee transactions (look at `Config / Finance / Transaction categories`)
    'transaction_fee_category' => 5,

    // Group bank statements by `month` or `day`
    'bank_statements_group' => 'month',

    // CookieValidationKey
    'cookieValidationKey' => ''
];
