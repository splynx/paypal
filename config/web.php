<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/paypal',
            ],
            'user' => [
                'identityClass' => 'splynx\models\Customer',
            ],
            'paypal' => [
                'class' => '\app\components\PayPal',
                'clientId' => $params['clientId'],
                'clientSecret' => $params['clientSecret'],
                'isProduction' => $params['isProduction'],

                // This is config file for the PayPal system
                'config' => [
                    'http.ConnectionTimeOut' => 30,
                    'http.Retry' => 1,
                    'mode' => 'sandbox',    // sandbox | live
                    'log.LogEnabled' => YII_DEBUG ? 1 : 0,
                    'log.FileName' => '@runtime/logs/paypal.log',
                    'log.LogLevel' => 'FINE',   // FINE | INFO | WARN | ERROR
                ],
            ],
        ],
    ];
};
