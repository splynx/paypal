<?php

namespace app\components;

use splynx\models\Customer;
use splynx\helpers\ApiHelper;
use yii;
use yii\base\Component;
use yii\base\UserException;

class ValidateConfig extends Component
{
    public static function checkAll($id_customer = null)
    {
        self::checkPayPal($id_customer);
        self::checkApi();
    }

    public static function checkPayPal($id_customer = null)
    {
        $params = Yii::$app->params;
        $clientIdAndSecret = self::loadClientIdAndSecretFromConfig($id_customer);

        if (!isset($clientIdAndSecret['clientId']) or $clientIdAndSecret['clientId'] == '') {
            throw new UserException('Error: clientId is not set. Please check your params.php');
        }

        if (!isset($clientIdAndSecret['clientSecret']) or $clientIdAndSecret['clientSecret'] == '') {
            throw new UserException('Error: clientSecret is not set. Please check your params.php');
        }

        if (!isset($params['currency']) or $params['currency'] == '') {
            throw new UserException('Error: currency is not entered. Please check your params.php');
        }

        if (!isset($params['splynx_url']) or $params['splynx_url'] == '') {
            throw new UserException('Error: splynx_url is not set. Please check your params.php');
        }

        if (strpos($params['splynx_url'], 'http://') !== 0 and strpos($params['splynx_url'], 'https://') !== 0) {
            throw new UserException('Error: splynx_url must start with `http://` or `https://`. Please check your params.php');
        }

        if (substr($params['splynx_url'], -1) == '/') {
            throw new UserException('Error: splynx_url must be without last slash. Please check your params.php');
        }
    }

    public static function checkApi()
    {
        $params = Yii::$app->params;
        if (!isset($params['api_key']) or $params['api_key'] == '') {
            throw new UserException('Error: api_key is not set. Please check your params.php');
        }

        if (!isset($params['api_secret']) or $params['api_secret'] == '') {
            throw new UserException('Error: api_secret is not set. Please check your params.php');
        }

        if (!isset($params['api_domain']) or $params['api_domain'] == '') {
            throw new UserException('Error: api_domain is not set. Please check your params.php');
        }

        if (strpos($params['api_domain'], 'http://') !== 0 and strpos($params['api_domain'], 'https://') !== 0) {
            throw new UserException('Error: api_domain must start with `http://` or `https://`. Please check your params.php');
        }

        if (substr($params['api_domain'], -1) != '/') {
            throw new UserException('Error: api_domain must be with last slash. Please check your params.php');
        }

        $result = ApiHelper::getInstance()->search('admin/customers/customer/', ['limit' => 1]);
        if ($result['result'] == false or empty($result['response'])) {
            throw new UserException('Error: Api call error. Please check your param.php');
        }
    }

    private static $_clientIdAndSecret;

    public static function loadClientIdAndSecretFromConfig($id_customer = null)
    {
        if (self::$_clientIdAndSecret === null) {
            /**
             * @var $customer Customer
             */
            if ($id_customer == null) {
                $customer = Yii::$app->getUser()->getIdentity();
            } else {
                $customer = Customer::findById($id_customer);
            }
            $clientIdAndSecret = [
                'clientId' => Yii::$app->params['clientId'],
                'clientSecret' => Yii::$app->params['clientSecret']
            ];
            if ($customer == null) {
                return self::$_clientIdAndSecret = $clientIdAndSecret;
            }
            $partner_id = $customer->partner_id;
            if (!isset(Yii::$app->params['partners_account'][$partner_id])) {
                return self::$_clientIdAndSecret = $clientIdAndSecret;
            }
            $partnerAccount = Yii::$app->params['partners_account'][$partner_id];

            if (!isset($partnerAccount['clientId'])
                or !isset($partnerAccount['clientSecret'])
            ) {
                return self::$_clientIdAndSecret = $clientIdAndSecret;
            }
            if (!empty($partnerAccount['clientId']) and !empty($partnerAccount['clientSecret'])) {
                $clientIdAndSecret['clientId'] = $partnerAccount['clientId'];
                $clientIdAndSecret['clientSecret'] = $partnerAccount['clientSecret'];
            }
            self::$_clientIdAndSecret = $clientIdAndSecret;
        }
        return self::$_clientIdAndSecret;
    }
}
