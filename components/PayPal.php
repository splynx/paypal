<?php
/**
 * File PayPal.php
 *
 * Based on https://github.com/kongoon/yii2-paypal
 *
 * @author Marcio Camello <marciocamello@outlook.com>
 * @author Manop Kongoon <kongoon@hotmail.com>
 *
 * @see https://github.com/paypal/rest-api-sdk-php/blob/master/sample/
 * @see https://developer.paypal.com/webapps/developer/applications/accounts
 */

namespace app\components;

define('PP_CONFIG_PATH', __DIR__);

use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use yii;
use yii\base\Component;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

class PayPal extends Component
{
    //region Mode (production/development)
    const MODE_SANDBOX = 'sandbox';
    const MODE_LIVE = 'live';
    //endregion

    //region Log levels
    /*
     * Logging level can be one of FINE, INFO, WARN or ERROR.
     * Logging is most verbose in the 'FINE' level and decreases as you proceed towards ERROR.
     */
    const LOG_LEVEL_FINE = 'FINE';
    const LOG_LEVEL_INFO = 'INFO';
    const LOG_LEVEL_WARN = 'WARN';
    const LOG_LEVEL_ERROR = 'ERROR';
    //endregion

    //region API settings
    public $clientId;
    public $clientSecret;
    public $isProduction = false;
    public $currency = 'USD';
    public $config = [];
    public $customer_id;

    /** @var ApiContext */
    private $_apiContext = null;

    /**
     * @setConfig
     * _apiContext in init() method
     */
    public function init()
    {
        $clientIdSecret = ValidateConfig::loadClientIdAndSecretFromConfig($this->customer_id);

        $this->clientId = $clientIdSecret['clientId'];
        $this->clientSecret = $clientIdSecret['clientSecret'];
        $this->isProduction = Yii::$app->params['isProduction'];

        $this->setConfig();
    }

    /**
     * @getApiContext
     * getApiContext
     */
    public function getApiContext()
    {
        return $this->_apiContext;
    }

    /**
     * @inheritdoc
     */
    private function setConfig()
    {
        // ### Api context
        // Use an ApiContext object to authenticate
        // API calls. The clientId and clientSecret for the
        // OAuthTokenCredential class can be retrieved from
        // developer.paypal.com

        $this->_apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->clientId,
                $this->clientSecret
            )
        );

        // #### SDK configuration

        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file
        // based configuration
        $this->_apiContext->setConfig(ArrayHelper::merge(
            [
                'mode' => $this->isProduction ? self::MODE_LIVE : self::MODE_SANDBOX, // development (sandbox) or production (live) mode
                'http.ConnectionTimeOut' => 30,
                'http.Retry' => 1,
                'log.LogEnabled' => YII_DEBUG ? 1 : 0,
                'log.FileName' => Yii::getAlias('@runtime/logs/paypal.log'),
                'log.LogLevel' => self::LOG_LEVEL_FINE,
                'validation.level' => 'log',
                'cache.enabled' => 'true',
                'cache.FileName' => Yii::getAlias('@runtime/cache/paypal/auth.cache'),
            ], $this->config)
        );

        // Set file name of the log if present
        if (isset($this->config['log.FileName'])
            && isset($this->config['log.LogEnabled'])
            && ((bool)$this->config['log.LogEnabled'] == true)
        ) {
            $logFileName = \Yii::getAlias($this->config['log.FileName']);

            if ($logFileName) {
                if (!file_exists($logFileName)) {
                    if (!touch($logFileName)) {
                        throw new ErrorException('Can\'t create paypal.log file at: ' . $logFileName);
                    }
                }
            }

            $this->config['log.FileName'] = $logFileName;
        }

        return $this->_apiContext;
    }

    public $redirectUrl;
    public $paymentId;

    public function create($amountMoney, $title, $type, $partner, $isDirect = false, $id = null)
    {
        // ### Payer
        // A resource representing a Payer that funds a payment
        // For paypal account payments, set payment method
        // to 'paypal'.
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // ### Itemized information
        // (Optional) Lets you specify item wise
        // information
        if (isset(Yii::$app->params['partnerCurrency'][$partner])) {
            $currency = Yii::$app->params['partnerCurrency'][$partner];
        } else {
            $currency = Yii::$app->params['currency'];
        }

        $item = new Item();
        $item->setName($title)
            ->setCurrency($currency)
            ->setQuantity(1)
            ->setPrice($amountMoney);

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        // ### Additional payment details
        // Use this optional field to set additional
        // payment information such as tax, shipping
        // charges etc.
//        $details = new Details();
//        $details->setShipping('1.20')
//            ->setTax('1.30')
//            ->setSubtotal('17.50');

        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency($currency)
            ->setTotal($amountMoney);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it.
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($title);
        if ($type == 'invoice' or $type == 'request') {
            $transaction->setInvoiceNumber($type . '-' . $id);
        }

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after
        // payment approval/ cancellation.
        $baseUrl = Yii::$app->params['splynx_url'];
        $redirectUrls = new RedirectUrls();

        if ($type == 'invoice' or $type == 'request') {
            $successUrl = $baseUrl . yii\helpers\Url::to(['/' . ($isDirect ? 'direct-' : '') . 'pay-' . $type,
                    'status' => 'success',
                    'item_id' => $id
                ]);
            $cancelUrl = $baseUrl . yii\helpers\Url::to(['/' . ($isDirect ? 'direct-' : '') . 'pay-' . $type,
                    'status' => 'cancel',
                    'item_id' => $id
                ]);
        } else {
            $successUrl = $baseUrl . yii\helpers\Url::to(['/result',
                    'status' => 'success'
                ]);
            $cancelUrl = $baseUrl . yii\helpers\Url::to(['/result',
                    'status' => 'cancel'
                ]);
        }

        $redirectUrls->setReturnUrl($successUrl)
            ->setCancelUrl($cancelUrl);

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $payment->create($this->_apiContext);
        } catch (PayPalConnectionException $e) {
            echo "Exception: " . $e->getMessage() . PHP_EOL;
            var_dump($e->getData());
            exit(1);
        }

        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getLinks()
        // method

        $this->paymentId = $payment->getId();

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $this->redirectUrl = $link->getHref();
                return true;
            }
        }

        // ### Redirect buyer to PayPal website
        // Save the payment id so that you can 'complete' the payment
        // once the buyer approves the payment and is redirected
        // back to your website.
        //
        // It is not a great idea to store the payment id
        // in the session. In a real world app, you may want to
        // store the payment id in a database.

//        $_SESSION['paymentId'] = $payment->getId();

//        if (isset($redirectUrl)) {
//            header("Location: $redirectUrl");
//            exit;
//        }
        return false;
    }

    public $response;
    /** @var Payment */
    public $result;
    public $error;

    public function execute($paymentId, $payerId)
    {
        // Get the payment Object by passing paymentId
        // payment id was previously stored in session in
        // CreatePaymentUsingPayPal.php
        $payment = Payment::get($paymentId, $this->_apiContext);

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        //Execute the payment
        // (See bootstrap.php for more on `ApiContext`)
        try {
            $this->result = $payment->execute($execution, $this->_apiContext);

            if ($this->result->getState() == 'approved') {
                return true;
            } else {
                return false;
            }

        } catch (PayPalConnectionException $e) {
            $this->error = $this->getErrorMessage($e->getData());
            return false;
        }
    }

    public function get($paymentId)
    {
        return Payment::get($paymentId, $this->_apiContext);
    }

    public function getResponse()
    {
        $this->response = $this->result->toArray();
        return $this->response;
    }

    public function getAmount()
    {
        $transactions = $this->result->getTransactions();
        $transaction = $transactions[0];
        $amount = $transaction->getAmount();
        $total = $amount->getTotal();
        return $total;
    }

    /**
     * Return response error message from PayPal
     *
     * @param $string
     * @return string
     */
    public function getErrorMessage($string)
    {
        $response = json_decode($string, true);
        if (isset($response['message'])) {
            return $response['message'];
        } else {
            return 'Unknown error';
        }
    }
}
